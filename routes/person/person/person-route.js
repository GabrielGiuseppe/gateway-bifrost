const CommonsValidation = require('../../../functions/commons/commons-validation');

module.exports = {
    basePath: '/pessoa',
    description: 'Pessoa API',
    version: 'V1',
    services: [
        {
            name: 'BuscarPessoaUuid',
            description: 'Buscar Pessoa por Uuid',
            method: 'GET',
            route: '/:uuid',
            responseHeaders: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            flow: [
                ...CommonsValidation.verifyAccess,
                {
                    resultVariable: '.',
                    skipErrors: false,
                    operation: {
                        clientName: 'personAPIClient',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8',
                        },
                        type: 'http',
                        method: 'GET',
                        path: 'pessoa/{{params.uuid}}',
                    }
                },
                ...CommonsValidation.ecryptData,
            ]
        },
        {
            name: 'ListarPessoa',
            description: 'Listar Pessoa',
            method: 'GET',
            route: '/',
            responseHeaders: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            flow: [
                ...CommonsValidation.verifyAccess,
                {
                    resultVariable: '.',
                    skipErrors: false,
                    operation: {
                        clientName: 'personAPIClient',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8',
                        },
                        type: 'http',
                        method: 'GET',
                        path: 'pessoa',
                    }
                },
                ...CommonsValidation.ecryptData,
            ]
        },
        {
            name: 'InserirPessoa',
            description: 'Inserir Pessoa',
            method: 'POST',
            route: '/',
            responseHeaders: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            flow: [
                ...CommonsValidation.verifyAccess,
                {
                    resultVariable: '.',
                    skipErrors: false,
                    operation: {
                        clientName: 'personAPIClient',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8',
                        },
                        type: 'http',
                        method: 'POST',
                        path: 'pessoa',
                    }
                },
                ...CommonsValidation.ecryptData,
            ]
        },
        {
            name: 'InserirPessoaLote',
            description: 'Inserir Pessoa em Lote',
            method: 'POST',
            route: '/lote',
            responseHeaders: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            flow: [
                ...CommonsValidation.verifyAccess,
                {
                    resultVariable: '.',
                    skipErrors: false,
                    operation: {
                        clientName: 'personAPIClient',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8',
                        },
                        type: 'http',
                        method: 'POST',
                        path: 'pessoa/lote',
                    }
                },
                ...CommonsValidation.ecryptData,
            ]
        },
        {
            name: 'AtualizarPessoaUuid',
            description: 'Atualizar Pessoa Uuid',
            method: 'PUT',
            route: '/:uuid',
            responseHeaders: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            flow: [
                ...CommonsValidation.verifyAccess,
                {
                    resultVariable: '.',
                    skipErrors: false,
                    operation: {
                        clientName: 'personAPIClient',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8',
                        },
                        type: 'http',
                        method: 'PUT',
                        path: 'pessoa/{{params.uuid}}',
                    }
                },
                ...CommonsValidation.ecryptData,
            ]
        }
    ]
};