'use strict';

const Util = require('bifrost-core').Util;
const Const = require("bifrost-core").Const;
const { ApiGatewayError, ValidationError, SecurityError, CreateSimpleError, NotFoundError } = require('bifrost-core').Errors;
const ClientFactory = require("bifrost-core").ClientFactory;
const backofficeClient = ClientFactory.createByName("backofficeAPIClient");
const ACTIONS = require('../consts/validate-const');
const Joi = require('@hapi/joi');
const Language = require('../consts/commons-msg');

class ValidateService {

    constructor(context) {
        this.context = context;
    };

    async validateRequest() {

        Util.info('Begin Create Auth Token');

        const hasValidate = this.context.headers["x-validate"];
        const action = this.context.service.method.toUpperCase();

        if (hasValidate === 'false' || action === 'GET') {
            return this.context;
        }

        const user = this.context.security.userData;
        const module = this.context.service.group.basePath;
        const systemAction = ACTIONS.filter(x => x.verb === action)[0];
        const parameters = await this.getParameters(user.cliente.id, module, systemAction);

        if (!parameters || parameters.length === 0) {
            return this.context;
        }

        await this.validateBody(parameters, this.context.body, module);

        Util.info('Finish Create Auth Token');

        return this.context;
    };

    async validateBody(parameters, body, module) {

        const validators = {};

        parameters.forEach(parameter => {
            this.createValidator(parameter, parameters, validators);
        });

        const validator = Joi.object(validators).required().unknown(true).messages(Language(`Informações do ${module}`));

        const result = validator.validate(body, { abortEarly: false, stripUnknown: true });

        if (result.error) {
            const detail = result.error.details[0];
            throw new ValidationError(detail.message, CreateSimpleError(detail.message));
        }
    }

    createValidator(parameter, parameters, validators) {
        let joi = Joi;

        if (parameter.tipo === 'STRING') {
            joi = joi.string().trim();
        }
        if (parameter.tipo === 'BOOLEAN') {
            joi = joi.boolean();
        }
        if (parameter.tipo === 'INTEGER') {
            joi = joi.number();
        }
        if (parameter.tipo === 'DOUBLE') {
            joi = joi.number();
        }
        if (parameter.tipo === 'DECIMAL') {
            joi = joi.number();
        }
        if (parameter.tipo === 'OBJECT') {
            insideParameters = parameters.filter(x => x.id === parameter && parameter.id);
            const insideValidators = {};
            insideParameters.forEach(insideParameter => {
                this.createValidator(insideParameter, insideParameters, insideValidators);
            });
            joi = joi.object(insideValidators);
        }
        if (parameter.tipo === 'ARRAY') {
            insideParameters = parameters.filter(x => x.id === parameter && parameter.id);
            const insideValidators = {};
            insideParameters.forEach(insideParameter => {
                this.createValidator(insideParameter, insideParameters, insideValidators);
            });
            joi = joi.array().items((insideValidators));
        }

        if (parameter.obrigatoriedade === 'YES') {
            joi = joi.required();
        } else {
            joi = joi.allow('').allow(null);
        }

        if (parameter.tamanho) {
            joi = joi.max(parseInt(parameter.tamanho));
        }

        if (parameter.valorPadrao) {
            joi = joi.default(parameter.valorPadrao);
        }

        validators[parameter.campo] = joi.messages(Language(parameter.campo));
    }

    async getParameters(clientId, module, action) {

        return new Promise(async (resolved, rejected) => {

            await backofficeClient.executePromise({
                headers: {
                    [Const.HEADERS.CONTENT_TYPE]: Const.JSON_CONTENT_TYPE_VALUE,
                    'client_id': clientId
                },
                path: `/parametro/${module}/${action.action}`
            }).then((response) => {

                const result = JSON.parse(response.body);

                resolved(result.dados);
            }).catch((err) => {
                Util.error(`Error to get credential for username: ${context.body.username}`, err);
                resolved();
            });
        });
    };
};

module.exports = ValidateService;
