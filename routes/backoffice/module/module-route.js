const CommonsValidation = require('../../../functions/commons/commons-validation');

module.exports = {
    basePath: '/modulo',
    description: 'Modulo API',
    version: 'V1',
    services: [
        {
            name: 'BuscarModulo',
            description: 'Buscar Modulo por Uuid',
            method: 'GET',
            route: '/:uuid',
            responseHeaders: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            flow: [
                ...CommonsValidation.verifyAccess,
                {
                    resultVariable: '.',
                    skipErrors: false,
                    operation: {
                        clientName: 'backofficeAPIClient',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8',
                        },
                        type: 'http',
                        method: 'GET',
                        path: 'modulo/{{params.uuid}}',
                    }
                },
                ...CommonsValidation.ecryptData,
            ]
        },
        {
            name: 'ListarModulo',
            description: 'Listar Modulo',
            method: 'GET',
            route: '/',
            responseHeaders: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            flow: [
                ...CommonsValidation.verifyAccess,
                {
                    resultVariable: '.',
                    skipErrors: false,
                    operation: {
                        clientName: 'backofficeAPIClient',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8',
                        },
                        type: 'http',
                        method: 'GET',
                        path: 'modulo',
                    }
                },
                ...CommonsValidation.ecryptData,
            ]
        },
        {
            name: 'InserirModulo',
            description: 'Inserir Modulo',
            method: 'POST',
            route: '/',
            responseHeaders: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            flow: [
                ...CommonsValidation.verifyAccess,
                {
                    resultVariable: '.',
                    skipErrors: false,
                    operation: {
                        clientName: 'backofficeAPIClient',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8',
                        },
                        type: 'http',
                        method: 'POST',
                        path: 'modulo',
                    }
                },
                ...CommonsValidation.ecryptData,
            ]
        },
        {
            name: 'AtualizarModulo',
            description: 'Atualizar Modulo',
            method: 'PUT',
            route: '/:uuid',
            responseHeaders: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            flow: [
                ...CommonsValidation.verifyAccess,
                {
                    resultVariable: '.',
                    skipErrors: false,
                    operation: {
                        clientName: 'backofficeAPIClient',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8',
                        },
                        type: 'http',
                        method: 'PUT',
                        path: 'modulo/{{params.uuid}}',
                    }
                },
                ...CommonsValidation.ecryptData,
            ]
        }
    ]
};