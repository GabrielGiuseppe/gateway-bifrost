const CommonsValidation = require('../../../functions/commons/commons-validation');

module.exports = {
    basePath: '/dado_pessoal',
    description: 'Dado Pessoal API',
    version: 'V1',
    services: [
        {
            name: 'BuscarDadoPessoalUuid',
            description: 'Buscar Dado Pessoa por Uuid',
            method: 'GET',
            route: '/:uuid',
            responseHeaders: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            flow: [
                ...CommonsValidation.verifyAccess,
                {
                    resultVariable: '.',
                    skipErrors: false,
                    operation: {
                        clientName: 'personAPIClient',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8',
                        },
                        type: 'http',
                        method: 'GET',
                        path: 'dado_pessoal/{{params.uuid}}',
                    }
                },
                ...CommonsValidation.ecryptData,
            ]
        },
        {
            name: 'ListarDadoPessoal',
            description: 'Listar Dado Pessoal',
            method: 'GET',
            route: '/',
            responseHeaders: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            flow: [
                ...CommonsValidation.verifyAccess,
                {
                    resultVariable: '.',
                    skipErrors: false,
                    operation: {
                        clientName: 'personAPIClient',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8',
                        },
                        type: 'http',
                        method: 'GET',
                        path: 'dado_pessoal',
                    }
                },
                ...CommonsValidation.ecryptData,
            ]
        },
        {
            name: 'InserirDadoPessoal',
            description: 'Inserir Dado Pessoal',
            method: 'POST',
            route: '/',
            responseHeaders: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            flow: [
                ...CommonsValidation.verifyAccess,
                {
                    resultVariable: '.',
                    skipErrors: false,
                    operation: {
                        clientName: 'personAPIClient',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8',
                        },
                        type: 'http',
                        method: 'POST',
                        path: 'dado_pessoal',
                    }
                },
                ...CommonsValidation.ecryptData,
            ]
        },
        {
            name: 'InserirDadoPessoalLote',
            description: 'Inserir Dado Pessoal em Lote',
            method: 'POST',
            route: '/lote',
            responseHeaders: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            flow: [
                ...CommonsValidation.verifyAccess,
                {
                    resultVariable: '.',
                    skipErrors: false,
                    operation: {
                        clientName: 'personAPIClient',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8',
                        },
                        type: 'http',
                        method: 'POST',
                        path: 'dado_pessoal/lote',
                    }
                },
                ...CommonsValidation.ecryptData,
            ]
        },
        {
            name: 'AtualizarDadoPessoalUuid',
            description: 'Atualizar Dado Pessoal Uuid',
            method: 'PUT',
            route: '/:uuid',
            responseHeaders: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            flow: [
                ...CommonsValidation.verifyAccess,
                {
                    resultVariable: '.',
                    skipErrors: false,
                    operation: {
                        clientName: 'personAPIClient',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8',
                        },
                        type: 'http',
                        method: 'PUT',
                        path: 'dado_pessoal/{{params.uuid}}',
                    }
                },
                ...CommonsValidation.ecryptData,
            ]
        }
    ]
};