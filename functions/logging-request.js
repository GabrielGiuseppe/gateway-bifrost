'use strict';

var Util = require('bifrost-core').Util;

exports.handle = (context) => {
    Util.info('Payload ' + Util.stringify(context.body));
}