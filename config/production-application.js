module.exports = {
    appName: process.env.appName || 'bifrost',
    requestTimeout: process.env.requestTimeout || 20000,
    requestPerSecond: process.env.requestPerSecond || 40,
    memoryCheckInterval: process.env.memoryCheckInterval || 60000,
    serverPort: process.env.serverPort || 9010,
    security: {
        secret: 'bifrost-secret'
    },
    basePathFunction: process.cwd(),
    logger: {
        debug: process.env.loggerDebug || false,
        transports: {
            Console: {
                colorize: 'all',
                level: process.env.loggerLevel || 'info',
                json: false,
                timestamp: true
            }
        }
    },
    redisCache: {
        reader: {
            host: "localhost",
            port: 6379,
            connectTimeout: 9000
        },
        writer: {
            host: "localhost",
            port: 6379,
            connectTimeout: 9000
        }
    }
};