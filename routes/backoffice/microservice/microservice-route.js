const CommonsValidation = require('../../../functions/commons/commons-validation');

module.exports = {
    basePath: '/microservico',
    description: 'Microserviço API',
    version: 'V1',
    services: [
        {
            name: 'BuscarMicroserviço',
            description: 'Buscar Microserviço por Uuid',
            method: 'GET',
            route: '/:uuid',
            responseHeaders: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            flow: [
                ...CommonsValidation.verifyAccess,
                {
                    resultVariable: '.',
                    skipErrors: false,
                    operation: {
                        clientName: 'backofficeAPIClient',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8',
                        },
                        type: 'http',
                        method: 'GET',
                        path: 'microservico/{{params.uuid}}',
                    }
                },
                ...CommonsValidation.ecryptData,
            ]
        },
        {
            name: 'ListarMicroserviço',
            description: 'Listar Microserviço',
            method: 'GET',
            route: '/',
            responseHeaders: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            flow: [
                ...CommonsValidation.verifyAccess,
                {
                    resultVariable: '.',
                    skipErrors: false,
                    operation: {
                        clientName: 'backofficeAPIClient',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8',
                        },
                        type: 'http',
                        method: 'GET',
                        path: 'microservico',
                    }
                },
                ...CommonsValidation.ecryptData,
            ]
        },
        {
            name: 'InserirMicroserviço',
            description: 'Inserir Microserviço',
            method: 'POST',
            route: '/',
            responseHeaders: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            flow: [
                ...CommonsValidation.verifyAccess,
                {
                    resultVariable: '.',
                    skipErrors: false,
                    operation: {
                        clientName: 'backofficeAPIClient',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8',
                        },
                        type: 'http',
                        method: 'POST',
                        path: 'microservico',
                    }
                },
                ...CommonsValidation.ecryptData,
            ]
        },
        {
            name: 'AtualizarMicroserviço',
            description: 'Atualizar Microserviço',
            method: 'PUT',
            route: '/:uuid',
            responseHeaders: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            flow: [
                ...CommonsValidation.verifyAccess,
                {
                    resultVariable: '.',
                    skipErrors: false,
                    operation: {
                        clientName: 'backofficeAPIClient',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8',
                        },
                        type: 'http',
                        method: 'PUT',
                        path: 'microservico/{{params.uuid}}',
                    }
                },
                ...CommonsValidation.ecryptData,
            ]
        }
    ]
};