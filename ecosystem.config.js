module.exports = {
    apps: [{
        name: "bifrost",
        script: "server.js",
        instances: 3,
        exec_mode: "cluster"
    }]
}