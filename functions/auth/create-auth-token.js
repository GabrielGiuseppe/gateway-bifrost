'use strict';

const AuthService = require('../../services/auth-service');

exports.handle = async (context) => {

    return new AuthService(context).createToken();

};