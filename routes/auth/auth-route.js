const CommonsValidation = require('../../functions/commons/commons-validation');

module.exports = {
    basePath: '/auth',
    description: 'Auth API',
    version: 'V1',
    services: [
        {
            name: 'login',
            description: 'Login',
            method: 'POST',
            route: '/login',
            responseHeaders: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            flow: [
                {
                    resultVariable: '.',
                    skipErrors: false,
                    operation: {
                        clientName: 'backofficeAPIClient',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8',
                        },
                        type: 'http',
                        method: 'POST',
                        path: 'auth/login',
                    }
                },
                {
                    operation: {
                        dir: 'auth',
                        functionName: 'create-auth-token',
                        type: 'function'
                    }
                },
                ...CommonsValidation.ecryptData,
            ]
        }
    ]
};