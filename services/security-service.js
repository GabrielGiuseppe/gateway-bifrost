'use strict';

const Util = require('bifrost-core').Util;
const { ApiGatewayError, ValidationError, SecurityError, CreateCustomError, NotFoundError } = require('bifrost-core').Errors;
const NodeRSA = require('node-rsa');

class SecurityService {

    constructor(context) {
        this.context = context;
    };

    async encrypt() {

        Util.info('Begin Create Encrypt Data');

        const hasEncryption = this.context.headers["x-encryption"];
        const result = this.context.result;

        if (hasEncryption === 'false') {
            return result;
        }

        const user = this.context.security.userData || result.dados;
        const key = new NodeRSA(user.chavePublica, "pkcs8-public-pem");
        const resultEncrypted = key.encrypt(result.dados, "base64");
        //this.context.resultEncrypted = {
        //    sucesso: result.sucesso,
        //    chavePublica: user.chavePublica,
        //    dados: resultEncrypted,
        //};
        const response = {
            sucesso: result.sucesso,
            chavePublica: user.chavePublica,
            dados: resultEncrypted,
        };

        Util.info('Finish Create Encrypt Data');

        return response;
    };

    async decrypt() {

        Util.info('Begin Create Decrypt Data');

        const hasEncryption = this.context.headers["x-encryption"];
        const action = this.context.service.method.toUpperCase();

        if (hasEncryption === 'false' || action === 'GET') {
            return this.context;
        }

        const result = this.context.body;
        const user = this.context.security.userData;
        const privateKey = user.chavePrivada;
        const key = new NodeRSA(privateKey, "pkcs8-private-pem");
        const resultDencrypted = key.decrypt(result || result.dados, 'utf-8');
        this.context.body = resultDencrypted;

        Util.info('Finish Create Decrypt Data');

        return this.context;
    };

};

module.exports = SecurityService;
