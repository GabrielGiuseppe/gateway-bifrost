'use strict';

const ACTIONS = [
    {
        verb: 'POST',
        action: 'INSERT',
    },
    {
        verb: 'PUT',
        action: 'UPDATE',
    },
    {
        verb: 'DELETE',
        action: 'DELETE',
    },
    {
        verb: 'PATCH',
        action: 'UPDATE',
    },
];

module.exports = ACTIONS;
