'use strict';

const ecryptData = [
    {
        resultVariable: '.',
        skipErrors: false,
        propagateResult: false,
        operation: {
            dir: 'security',
            functionName: 'encrypt-data',
            type: 'function'
        }
    },
    //{
    //    resultVariable: '.',
    //    skipErrors: false,
    //    propagateResult: true,
    //    operation: {
    //        dir: 'cache',
    //        functionName: 'create-cache',
    //        type: 'function'
    //    }
    //}
];

const decryptData = [
    //{
    //    resultVariable: '.',
    //    skipErrors: false,
    //    propagateResult: false,
    //    operation: {
    //        dir: 'cache',
    //        functionName: 'get-cache',
    //        type: 'function'
    //    }
    //},
    {
        resultVariable: '.',
        skipErrors: false,
        propagateResult: false,
        operation: {
            dir: 'security',
            functionName: 'decrypt-data',
            type: 'function'
        }
    }   
];

const verifyAccess = [
    {
        resultVariable: '.',
        skipErrors: false,
        propagateResult: false,
        operation: {
            dir: 'auth',
            functionName: 'verify-auth-token',
            type: 'function'
        }
    },
    {
        resultVariable: '.',
        skipErrors: false,
        propagateResult: false,
        operation: {
            dir: 'permission',
            functionName: 'verify-permission',
            type: 'function'
        }
    },
    {
        resultVariable: '.',
        skipErrors: false,
        propagateResult: false,
        operation: {
            dir: 'validate',
            functionName: 'validate-request',
            type: 'function'
        }
    },
    ...decryptData
];


exports.verifyAccess = verifyAccess;
exports.ecryptData = ecryptData;
exports.decryptData = decryptData;