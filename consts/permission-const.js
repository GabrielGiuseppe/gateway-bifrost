'use strict';

const PERMISSIONS = [
    {
        action: 'GET',
        levelAccess: ['VIEWER', 'EDITOR', 'FULL']
    },
    {
        action: 'POST',
        levelAccess: ['EDITOR', 'FULL']
    },
    {
        action: 'PUT',
        levelAccess: ['EDITOR', 'FULL']
    },
    {
        action: 'DELETE',
        levelAccess: ['FULL']
    },
    {
        action: 'PATCH',
        levelAccess: ['EDITOR', 'FULL']
    }
];

module.exports = PERMISSIONS;
