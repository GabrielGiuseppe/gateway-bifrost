FROM node:latest

RUN mkdir -p /opt/bifrost
WORKDIR /opt/bifrost

# Install app dependencies
COPY package.json /opt/bifrost/

RUN npm install

# Bundle app source
COPY . /opt/bifrost

CMD ["node", "ecosystem.config.js"]