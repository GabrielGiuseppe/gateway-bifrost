'use strict';

const SecurityService = require('../../services/security-service');

exports.handle = async (context) => {

    return new SecurityService(context).decrypt();

};