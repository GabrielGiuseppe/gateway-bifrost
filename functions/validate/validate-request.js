'use strict';

const ValidateService = require('../../services/validate-service');

exports.handle = async (context) => {

    return new ValidateService(context).validateRequest();

};