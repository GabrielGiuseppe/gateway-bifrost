'use strict';

const CacheService = require('../../services/cache-service');

exports.handle = async (context, callbackStep) => {

    return new CacheService(context, callbackStep).getCache();

};