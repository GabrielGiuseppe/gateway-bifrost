const CommonsValidation = require('../../../functions/commons/commons-validation');

module.exports = {
    basePath: '/cbo_família',
    description: 'CBO Família API',
    version: 'V1',
    services: [
        {
            name: 'BuscarCboFamíliaUuid',
            description: 'Buscar CBO Família por Uuid',
            method: 'GET',
            route: '/:uuid',
            responseHeaders: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            flow: [
                ...CommonsValidation.verifyAccess,
                {
                    resultVariable: '.',
                    skipErrors: false,
                    operation: {
                        clientName: 'personAPIClient',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8',
                        },
                        type: 'http',
                        method: 'GET',
                        path: 'cbo_família/{{params.uuid}}',
                    }
                },
                ...CommonsValidation.ecryptData,
            ]
        },
        {
            name: 'ListarCboFamília',
            description: 'Listar CBO Família',
            method: 'GET',
            route: '/',
            responseHeaders: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            flow: [
                ...CommonsValidation.verifyAccess,
                {
                    resultVariable: '.',
                    skipErrors: false,
                    operation: {
                        clientName: 'personAPIClient',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8',
                        },
                        type: 'http',
                        method: 'GET',
                        path: 'cbo_família',
                    }
                },
                ...CommonsValidation.ecryptData,
            ]
        },
        {
            name: 'InserirCboFamília',
            description: 'Inserir CBO Família',
            method: 'POST',
            route: '/',
            responseHeaders: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            flow: [
                ...CommonsValidation.verifyAccess,
                {
                    resultVariable: '.',
                    skipErrors: false,
                    operation: {
                        clientName: 'personAPIClient',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8',
                        },
                        type: 'http',
                        method: 'POST',
                        path: 'cbo_família',
                    }
                },
                ...CommonsValidation.ecryptData,
            ]
        },
        {
            name: 'InserirCboFamíliaLote',
            description: 'Inserir CBO Família em Lote',
            method: 'POST',
            route: '/lote',
            responseHeaders: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            flow: [
                ...CommonsValidation.verifyAccess,
                {
                    resultVariable: '.',
                    skipErrors: false,
                    operation: {
                        clientName: 'personAPIClient',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8',
                        },
                        type: 'http',
                        method: 'POST',
                        path: 'cbo_família/lote',
                    }
                },
                ...CommonsValidation.ecryptData,
            ]
        },
        {
            name: 'AtualizarCboFamíliaUuid',
            description: 'Atualizar CBO Família Uuid',
            method: 'PUT',
            route: '/:uuid',
            responseHeaders: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            flow: [
                ...CommonsValidation.verifyAccess,
                {
                    resultVariable: '.',
                    skipErrors: false,
                    operation: {
                        clientName: 'personAPIClient',
                        headers: {
                            'Content-Type': 'application/json;charset=UTF-8',
                        },
                        type: 'http',
                        method: 'PUT',
                        path: 'cbo_família/{{params.uuid}}',
                    }
                },
                ...CommonsValidation.ecryptData,
            ]
        }
    ]
};