'use strict'

const APIGateway = require('bifrost-core').APIGateway;

const server = APIGateway.createServer();
server.start();