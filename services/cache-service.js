'use strict';

const Util = require('bifrost-core').Util;
const { ApiGatewayError, ValidationError, SecurityError, CreateCustomError, NotFoundError } = require('bifrost-core').Errors;
const NodeRSA = require('node-rsa');

const THIRTY_DAYS = 2.592e+9; // 30 dias
const LAST_UPDATE = "LAST-UPDATE";

class CacheService {

    constructor(context, callbackStep) {
        this.context = context;
        this.callbackStep = callbackStep;
    };

    async createCache() {

        Util.info('Begin Create Cache Data');

        const redis = this.context.orchestrator.redisClient;
        const lastUpdate = this.context.timers.started;
        const hasEncryption = this.context.headers["x-encryption"];
        let response = null;

        if (hasEncryption === 'true') {
            response = this.context.result.encrypt.resultEncrypted;
        } else {
            delete this.context.result.encrypt;
            response = this.context.result;
        }

        const result = this.context.result;
        const module = this.context.service.group.basePath;
        const key = Array.isArray(result.dados) ? 'LIST' : result.dados.id || result.dados.uuid;

        redis.put(module, key, response, THIRTY_DAYS);
        redis.put(module, LAST_UPDATE, lastUpdate, THIRTY_DAYS);

        Util.info('Finish Create Cache Data');

        return response;
    };

    async getCache() {

        Util.info('Begin Get Cache Data');
        const action = this.context.service.method.toUpperCase();

        if (action !== 'GET') {
            return this.context;
        }

        const redis = this.context.orchestrator.redisClient;
        const lastUpdate = this.context.timers.started;
        const module = this.context.service.group.basePath;
        const params = this.context.params;
        const query = this.context.query;
        const queries = Object.keys(query);
        let key = params.id || params.uuid || 'LIST';

        queries.forEach(e => {

            key += `-${e}-${query[e]}`;

        });

        const cacheData = await redis.getAsync(`${module}:${key}`);

        Util.info('Finish Get Cache Data');

        return this.callbackStep(cacheData);
    };

};

module.exports = CacheService;
