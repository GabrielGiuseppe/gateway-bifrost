'use strict';

const Util = require('bifrost-core').Util;
const { ApiGatewayError, ValidationError, SecurityError, CreateSimpleError, NotFoundError } = require('bifrost-core').Errors;
const PERMISSIONS = require('../consts/permission-const');

class PermissionService {

    constructor(context) {
        this.context = context;
    };

    async verifyPermission() {

        Util.info('Begin Create Auth Token');

        const hasPermission = this.context.headers["x-permission"];

        if (hasPermission === 'false') {
            return this.context;
        }

        const user = this.context.security.userData;
        const accessGroup = user.grupoAcesso;
        const permissions = accessGroup.permissoes.filter(x => x.status === 'ACTIVE');
        const module = this.context.service.group.basePath;
        const action = this.context.service.method.toUpperCase();
        const hasModule = permissions.filter(x => module.includes(x.modulo.nome));

        if (!hasModule) {
            throw new SecurityError('Argumentos inválidos', CreateSimpleError('Você não tem permissão para fazer isso'));
        }

        const permission = hasModule[0];
        const systemPermission = PERMISSIONS.filter(x => x.action === action)[0];
        const hasAccess = systemPermission.levelAccess.some(x => x === permission.nivelAcesso);

        if (!hasAccess) {
            throw new SecurityError('Argumentos inválidos', CreateSimpleError('Você não tem permissão para fazer isso'));
        }

        Util.info('Finish Create Auth Token');

        return this.context;
    };

};

module.exports = PermissionService;
