'use strict';

const CacheService = require('../../services/cache-service');

exports.handle = async (context) => {

    return new CacheService(context).createCache();

};