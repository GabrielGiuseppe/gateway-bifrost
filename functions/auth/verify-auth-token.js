'use strict';

const AuthService = require('../../services/auth-service');

exports.handle = async (context) => {

    return await new AuthService(context).verifyToken();

};