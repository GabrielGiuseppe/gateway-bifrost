'use strict';

const PermissionService = require('../../services/permission-service');

exports.handle = async (context) => {

    return new PermissionService(context).verifyPermission();

};