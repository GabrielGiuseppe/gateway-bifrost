
const AuthRoute = require('../routes/auth/auth-route');
const ClientRoute = require('../routes/backoffice/client/client-route');
const ConfigurationEmailRoute = require('../routes/backoffice/configuration-email/configuration-email-route');
const MicroserviceRoute = require('../routes/backoffice/microservice/microservice-route');
const ModuleRoute = require('../routes/backoffice/module/module-route');
const SubModuleRoute = require('../routes/backoffice/sub-module/sub-module-route');
const UserProfileRoute = require('../routes/backoffice/user-profile/user-profile-route');

const AddressRoute = require('../routes/utilities/address/address-route');
const AddressTypeRoute = require('../routes/utilities/address-type/address-type-route');
const BankAccountRoute = require('../routes/utilities/bank-account/bank-account-route');
const BankAccountTypeRoute = require('../routes/utilities/bank-account-type/bank-account-type-route');
const BankAgencyRoute = require('../routes/utilities/bank-agency/bank-agency-route');
const BankDataRoute = require('../routes/utilities/bank-data/bank-data-route');
const BankEntityRoute = require('../routes/utilities/bank-entity/bank-entity-route');
const CityRoute = require('../routes/utilities/city/city-route');
const CnhRoute = require('../routes/utilities/cnh/cnh-route');
const CountryRoute = require('../routes/utilities/country/country-route');
const CtpsRoute = require('../routes/utilities/ctps/ctps-route');
const DocumentRoute = require('../routes/utilities/document/document-route');
const EmailRoute = require('../routes/utilities/email/email-route');
const EmailTypeRoute = require('../routes/utilities/email-type/email-type-route');
const NeighborhoodRoute = require('../routes/utilities/neighborhood/neighborhood-route');
const TelephoneRoute = require('../routes/utilities/telephone/telephone-route');
const TelephoneCompanyRoute = require('../routes/utilities/telephone-company/telephone-company-route');
const TelephoneTypeRoute = require('../routes/utilities/telephone-type/telephone-type-route');
const PublicPlaceRoute = require('../routes/utilities/public-place/public-place-route');
const PublicPlaceTypeRoute = require('../routes/utilities/public-place-type/public-place-type-route');
const RgRoute = require('../routes/utilities/rg/rg-route');
const RicRoute = require('../routes/utilities/ric/ric-route');
const StateRoute = require('../routes/utilities/state/state-route');
const VoterRegistrationRoute = require('../routes/utilities/voter-registration/voter-registration-route');

const Affiliation = require('../routes/person/affiliation/affiliation-route');
const CboFamily = require('../routes/person/cbo-family/cbo-family-route');
const CboGreatGroup = require('../routes/person/cbo-great-group/cbo-great-group-route');
const CboOcupation = require('../routes/person/cbo-ocupation/cbo-ocupation-route');
const CboPrincipalSubgroup = require('../routes/person/cbo-principal-subgroup/cbo-principal-subgroup-route');
const CboSubgroup = require('../routes/person/cbo-subgroup/cbo-subgroup-route');
const CboSynonym = require('../routes/person/cbo-synonym/cbo-synonym-route');
const Nationality = require('../routes/person/nationality/nationality-route');
const OccupacionalData = require('../routes/person/occupational-data/occupational-data-route');
const Person = require('../routes/person/person/person-route');
const PersonalData = require('../routes/person/personal-data/personal-data-route');
const ProfessionalClass = require('../routes/person/professional-class/professional-class-route');
const ProfessionalClassEntity = require('../routes/person/professional-class-entity/professional-class-entity-route');
const SpecificData = require('../routes/person/specific-data/specific-data-route');

module.exports =
    [
        AuthRoute,
        ClientRoute,
        ConfigurationEmailRoute,
        MicroserviceRoute,
        ModuleRoute,
        SubModuleRoute,
        UserProfileRoute,
        AddressRoute,
        AddressTypeRoute,
        BankAccountRoute,
        BankAccountTypeRoute,
        BankAgencyRoute,
        BankDataRoute,
        BankEntityRoute,
        CityRoute,
        CnhRoute,
        CountryRoute,
        CtpsRoute,
        DocumentRoute,
        EmailRoute,
        EmailTypeRoute,
        NeighborhoodRoute,
        TelephoneRoute,
        TelephoneCompanyRoute,
        TelephoneTypeRoute,
        PublicPlaceRoute,
        PublicPlaceTypeRoute,
        RgRoute,
        RicRoute,
        StateRoute,
        VoterRegistrationRoute,
        Affiliation,
        CboFamily,
        CboGreatGroup,
        CboOcupation,
        CboPrincipalSubgroup,
        CboSubgroup,
        CboSynonym,
        Nationality,
        OccupacionalData,
        Person,
        PersonalData,
        ProfessionalClass,
        ProfessionalClassEntity,
        SpecificData,
    ];