'use strict';

const JWT = require('jsonwebtoken');
const Util = require('bifrost-core').Util;
const { ApiGatewayError, ValidationError, SecurityError, CreateCustomError, NotFoundError } = require('bifrost-core').Errors;
const { application: { security: { secret } } } = require('bifrost-core').Environment;

class AuthService {

    constructor(context) {
        this.context = context;
    };

    async verifyToken() {

        const token = this.context.headers.authorization;

        Util.info('stage=info method=AuthService.verifyToken', token);

        const verify = JWT.verify(token, secret);

        this.context.security.userData = verify.data;

        Util.info('Finish Verify Auth Token');

        return this.context;
    };

    async createToken() {

        Util.info('Begin Create Auth Token');

        const result = this.context.result;

        const token = JWT.sign({ data: result.dados }, secret, { expiresIn: '30d' });

        result.dados.token = token;

        Util.info('Finish Create Auth Token');

        return result
    };

};

module.exports = AuthService;
