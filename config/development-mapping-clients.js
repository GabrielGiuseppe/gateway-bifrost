module.exports = {
    personAPIClient: {
        description: 'Pessoa API',
        host: process.env.personAPIClient || 'http://10.5.0.7:8081',
        user: 'equiplano',
        password: 'equiplano',
        authentication: 'basic',
        poolSize: Infinity,
        timeout: 60000,
        healthCheck: 'monitor/health'
    },
    utilitiesAPIClient: {
        description: 'Utilitarios API',
        host: process.env.utilitiesAPIClient || 'http://10.5.0.8:8082',
        user: 'equiplano',
        password: 'equiplano',
        authentication: 'basic',
        poolSize: Infinity,
        timeout: 60000,
        healthCheck: 'monitor/health'
    },
    backofficeAPIClient: {
        description: 'Backoffice API',
        host: process.env.backofficeAPIClient || 'http://10.5.0.9:8083',
        user: 'equiplano',
        password: 'equiplano',
        authentication: 'basic',
        poolSize: Infinity,
        timeout: 60000,
        healthCheck: 'monitor/health'
    },
    entityAPIClient: {
        description: 'Entidade API',
        host: process.env.entityAPIClient || 'http://10.5.0.10:8084',
        user: 'equiplano',
        password: 'equiplano',
        authentication: 'basic',
        poolSize: Infinity,
        timeout: 60000,
        healthCheck: 'monitor/health'
    },
};